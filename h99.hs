-- | H-99: Ninety-Nine Haskell Problems
-- | Solutions by Cedric Villeneuve @vildric
-- | <vildric@gmail.com>
-- | Learn Haskell plz, http://learnyouahaskell.com

module H99 where 

-- Find the last element of a list
myLast :: [a] -> Maybe a
myLast [] = Nothing
myLast [x] = Just x
myLast (_:xs) = myLast xs

-- Find the last but one element of a list
myButLast :: [a] -> Maybe a
myButLast [] = Nothing
myButLast (x:_:[]) = Just x
myButLast (_:xs) = myButLast xs 

-- Find the K'th element of a list. The first element in the list is number 1.
elementAt [] _ = Nothing
elementAt _ 0 = Nothing
elementAt (x:_) 1 = Just x
elementAt (_:xs) n = elementAt xs (n - 1)


-- Find the number of elements of a list.
myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

-- Reverse a list.
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x] 

-- Find out whether a list is a palindrome. A palindrome can be read forward or backward; e.g. (x a m a x).
isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome [_] = True
isPalindrome (x:xs)
    | x /= last xs = False -- if the first and the last elements are not equal, how the list can be a palindrome?
    | otherwise = isPalindrome $ init xs 

-- Eliminate consecutive duplicates of list elements.
compress :: Eq a => [a] -> [a]
compress [] = [] 
compress [x] = [x]
compress (x:xs)
    | x /= head xs = x : compress xs 
    | otherwise = compress xs 
